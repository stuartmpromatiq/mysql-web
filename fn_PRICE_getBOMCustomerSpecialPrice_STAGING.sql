DELIMITER $$
CREATE DEFINER=`swdadmin`@`%` FUNCTION `fn_PRICE_getBOMCustomerSpecialPrice_STAGING`(`inpCustomerCode` VARCHAR(50),  `inpProductCode` VARCHAR(250), `inpQty` int) RETURNS varchar(20000) CHARSET latin1
BEGIN
	Declare StringResult varchar(20000);
    Declare StringBOMResult varchar(20000);
    Declare StringCompPrice varchar(1000);
    Declare StringDelimitedPrice varchar(1000);
    Declare ComponentPart varchar(50);
    Declare ComponentQty int;
	Declare ComponentRRP decimal(10,2); 
    Declare ComponentRetail decimal(10,2);  
    Declare ComponentSort int;
    Declare ComponentPrice decimal(10,2);
    Declare KitPrice decimal(10,2);
    Declare CalcKitPrice decimal(10,2);
    Declare KitRRP decimal(10,2); 
    Declare KitRetail decimal(10,2); 
    Declare UseBOM varchar(5);
    Declare BOMCount int;
    Declare CalcKitRRP decimal(10,2); 
    Declare CalcKitRetail decimal(10,2); 
    Declare ComponentDisc varchar(6);
	Declare KitDisc varchar(6);
	Declare ComponentPriceNo varchar(5);
	Declare KitPriceNo varchar(5);
    Declare ComponentRRPStr varchar(10);
    Declare KitRRPStr varchar(10);
    Declare KitRetailStr varchar(10);
    Declare ComponentPriceStr varchar(10);
    Declare KitPriceStr varchar(10);
    Declare KitPriceOverflow bit;
    -- Add specials
    Declare SpecialKitPriceStr varchar(10);
    Declare SpecialKitDisc varchar(6);
    Declare SpecialKitPriceNo varchar(5);
    Declare SpecialComponentPriceStr varchar(10);    
    Declare SpecialComponentDisc varchar(6);
    Declare SpecialComponentPriceNo varchar(5);
    Declare SpecialComponentPrice decimal(10,2);
    Declare SpecialKitPrice decimal(10,2);
    Declare SpecialCalcKitPrice decimal(10,2);
    Declare PriceRegion varchar(10);
    
    
    
	Declare Result Real;
    Declare done Int DEFAULT FALSE;
    
    /*This is for debugging only */
	Declare varDebugCustType varchar(10);
    Declare varDebugPrice2 varchar(10);
    Declare varDebugPrice3 varchar(10);
    Declare varDebugBasePrice varchar(10);
    Declare varDebugPriceChecks varchar(2000);
    /* End debug variables*/

    
    Declare BC Cursor For 
	Select SortOrder, ComponentCode, Quantity, Round(ComponentRRPExcl * 1.1,2) As ComponentRRP   
    From SWD_BOM Where KitNumber = inpProductCode
	  Order By SortOrder;      
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;  
    
    Select Count(*) As BomCount,  Round( Max(KitRRPExcl) * 1.1,2) As KitRRP, Round( Sum(ComponentRRPExcl) * 1.1,2) As CalcKitRRP 
	From SWD_BOM Where KitNumber = inpProductCode Into BOMCount, KitRRP, CalcKitRRP;
    
    -- Get price region
	Select dr_cust_type Into PriceRegion From SWD_Debtor_Master Where accountcode = inpCustomerCode ;
    
    Set StringBOMResult = '';
    Set KitPrice = 0;
    Set KitDisc = '0';
    Set KitPriceNo = '0';
    Set UseBOM = 'true';
    -- Specials
    Set SpecialKitPriceStr = '';
    Set SpecialKitPrice = 0;
    Set SpecialComponentDisc = '';
    Set SpecialComponentPriceNo = '';
    -- Kit Retail
    Set KitRetail = 0;
    Set varDebugPriceChecks = '';
    
    Set KitPriceOverflow = 0;
   
    /* Check for individual kit pricing*/
	If  BomCount = 0 Or KitRRP > 0 Then
    
		/* Treat as part if no BOM */
        -- If BomCount = 0 Then
			Select Round( Max(IfNull(prc_recommend_retail,prc_break_price3)) * 1.1,2)  Into KitRRP
			FROM SWD_Stock_Price
			Where prc_region_code = ''
			And stock_code =  inpProductCode;
            /* Add retail price*/
            If PriceRegion Like '%3P%' Then
				Select Round( Max(prc_break_price4) * 1.1,2)  Into KitRetail
				FROM SWD_Custom_Stock_Price
				Where prc_region_code = '3P'
				And stock_code =  inpProductCode;
			else
				Set KitRetail = KitRRP;
			End If;
        -- End If;
    
		/* treat as a part number*/
		/*Select fn_PRICE_getCustomerSpecialPrice (inpCustomerCode,inpProductCode,1) Into KitPrice;  */
        Select fn_PRICE_getDelimitedCustomerSpecialPrice_STAGING (inpCustomerCode,inpProductCode,inpQty) Into StringDelimitedPrice;
        /* Extract into price components */
        SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 1), ',', -1), ':', 2), ':', -1) Into KitPriceStr;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 2), ',', -1), ':', 2), ':', -1) Into KitRRPStr;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 3), ',', -1), ':', 2), ':', -1) Into KitDisc;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 4), ',', -1), ':', 2), ':', -1) Into KitPriceNo;
        SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 5), ',', -1), ':', 2), ':', -1) Into SpecialKitPriceStr;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 6), ',', -1), ':', 2), ':', -1) Into SpecialKitDisc;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 7), ',', -1), ':', 2), ':', -1) Into SpecialKitPriceNo;
        /*This is for debugging only */
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 8), ',', -1), ':', 2), ':', -1) Into varDebugCustType;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 9), ',', -1), ':', 2), ':', -1) Into varDebugPrice2;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 10), ',', -1), ':', 2), ':', -1) Into varDebugPrice3;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 11), ',', -1), ':', 2), ':', -1) Into varDebugBasePrice;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 12), ',', -1), ':', 2), ':', -1) Into varDebugPriceChecks;
        -- replace special characters
        Set varDebugPriceChecks = replace(replace(varDebugPriceChecks,'#',':'),'|',',');

		/* End debug variables*/       
        
        
       
        
        Select convert(KitPriceStr,decimal(10,2)) Into KitPrice;
        Select convert(SpecialKitPriceStr,decimal(10,2)) Into SpecialKitPrice;
        
        Set UseBOM = 'false';
        Set done = TRUE; 
    End If;
    
    If KitRRP = 0 Then
		Set KitRRP = CalcKitRRP;        
	End If;
  
    
    /* Now go through each BOM item */
    Set CalcKitPrice = 0;
    Set SpecialCalcKitPrice = 0;
    Set CalcKitRetail = 0;
    Open BC;
    read_loop: LOOP
		
		FETCH BC INTO ComponentSort,ComponentPart,ComponentQty,ComponentRRP;
		IF done THEN 
			LEAVE read_loop;
		END IF;
        
        
   
        /* Get component price*/
        Set ComponentPrice = 0;
        /*Select fn_PRICE_getCustomerSpecialPrice (inpCustomerCode,ComponentPart,1) Into ComponentPrice;*/
        
        Set ComponentPriceStr = '0';
        Set ComponentRRPStr = '0';
        Set ComponentDisc = '0';
        Set ComponentPriceNo = '0';
        Set StringDelimitedPrice = '';
        Select fn_PRICE_getDelimitedCustomerSpecialPrice_STAGING (inpCustomerCode,ComponentPart,inpQty) Into StringDelimitedPrice;
        /* Extract into price components */
        SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 1), ',', -1), ':', 2), ':', -1) Into ComponentPriceStr;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 2), ',', -1), ':', 2), ':', -1) Into ComponentRRPStr;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 3), ',', -1), ':', 2), ':', -1) Into ComponentDisc;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 4), ',', -1), ':', 2), ':', -1) Into ComponentPriceNo;
        SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 5), ',', -1), ':', 2), ':', -1) Into SpecialComponentPriceStr;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 6), ',', -1), ':', 2), ':', -1) Into SpecialComponentDisc;
		SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(StringDelimitedPrice, ',', 7), ',', -1), ':', 2), ':', -1) Into SpecialComponentPriceNo;
        
        Select convert(ComponentPriceStr,decimal(10,2)) Into ComponentPrice;
		Select convert(SpecialComponentPriceStr,decimal(10,2)) Into SpecialComponentPrice;
                     
        
        /* Check for an overflow */
        If ComponentPrice = 0 Or ComponentPrice > 20000 Then
			Set KitPriceOverflow = 1;
		End If;
        
        /* Add Part Retail Price*/
        If PriceRegion Like '%3P%' Then
			Select Round( Max(prc_break_price4) * 1.1,2)  Into ComponentRetail
			FROM SWD_Custom_Stock_Price
			Where prc_region_code = '3P'
			And stock_code =  ComponentPart;
		else
			Set ComponentRetail = ComponentRRP;
        End If;
        
        /* Default Component Retail to Component RRP if 0 */
		If IfNull(ComponentRetail,0) = 0 Then
			Set ComponentRetail = ComponentRRP;
        End If;
            
		
        /* default to force the kit RRP if 0 */
        If IfNull(ComponentRetail,0) = 0 Then
			Set ComponentRetail = 20000;
        End If;
        
            
        
        /* Don't display component prices that are invalid*/
		If ComponentRetail >= 20000 Or IfNull(ComponentRetail,0) = 0 Then
			Set ComponentRetail = 0;
        End If;   
        
		/* Calculate Kit Retail */
        Set CalcKitRetail = CalcKitRetail  +  (IfNull(ComponentRetail,0) * ComponentQty);
        
                    
         -- Special
        If IfNull(SpecialComponentPrice,0) = 0  Then
			Set SpecialCalcKitPrice = 0;
		Else
			If SpecialCalcKitPrice > 0 Or CalcKitPrice = 0 Then
				Set SpecialCalcKitPrice = SpecialCalcKitPrice + (IfNull(SpecialComponentPrice,0) * ComponentQty);
			End If;
        End If;
        
       
        /* Add checks to identify whether all parts are on special. pass on special prices to customer price*/
        Set CalcKitPrice = CalcKitPrice + (Case When IfNull(SpecialComponentPrice,0) <  IfNull(ComponentPrice,0) And SpecialComponentPrice > 0 Then  IfNull(SpecialComponentPrice,0) Else IfNull(ComponentPrice,0) End  * ComponentQty);
        
        
        /* Don't show cost+ negative discounts */
		If ComponentDisc < 0 Then
			Set ComponentDisc = 0;
		End If;
		
		If SpecialComponentDisc < 0 Then
			Set SpecialComponentDisc = 0;
		End If;
    
		/* Build JSON */
        Set StringCompPrice = Concat('{"sku":"',ComponentPart ,'",',
        '"rrp":',IfNull(Convert(ComponentRRP,CHAR(10) ),'0'),',',
        '"customer_price":',IfNull(Convert(ComponentPrice,CHAR(10) ),'0'),',',
        '"qty":',IfNull(Convert(ComponentQty,CHAR(2) ),'1'),',',
        '"discount":',IfNull(ComponentDisc,'0'),',',
        '"price_no":',IfNull(ComponentPriceNo,'0'),',',  
		'"special_price":',IfNull(Convert(SpecialComponentPrice,CHAR(10) ),'0'),',',
        '"special_discount":',IfNull(SpecialComponentDisc,'0'),',',
        '"special_price_no":',IfNull(SpecialComponentPriceNo,'0'),',',
        '"shopify_rrp":',IfNull(Convert(ComponentRetail,CHAR(10) ),'0'),
        '}');
    
		/* Set to BOM Result */
        If StringBOMResult = '' Then
			Set StringBOMResult = concat(',"bom":[',IfNull(StringCompPrice,''));
		else
			Set StringBOMResult = concat(StringBOMResult,',',StringCompPrice);
        End If;

   
    END LOOP read_loop;
	CLOSE BC;
    
    /* End BOM Result */
	If StringBOMResult <> '' Then
		Set StringBOMResult = concat(StringBOMResult,CHAR(10),CHAR(13),'   ]');
	End If;
    
    
    /* Check for an overflow */
	If KitPriceOverflow = 1 Then
		Set CalcKitPrice = 0;
	End If;
    
    /* Fix Kit Price */
    If KitPrice = 0 Then
		Set KitPrice = CalcKitPrice;
	End If;
    
    If SpecialKitPrice = 0 Then
		Set SpecialKitPrice = SpecialCalcKitPrice;
	End If;
    
    If KitRetail = 0 Then
		Set KitRetail = CalcKitRetail;
	End If;
    

    
    /* Restrict Kit RRP to not go over 30000 */
    If KitRRP > 30000 Then
		Set KitRRP = 0;
    End If;
    
    /* ensure no nulls */
    If KitRRP Is Null Then
		Set KitRRP = 0;
    End If;
    
    /* Use RRP if the kit retail is 0 */
    If IfNull(KitRetail,0) = 0 Then
		Set KitRetail = KitRRP;
    End If;
    
    /* Restrict Kit RRP to not go over 30000 */
	If KitRetail > 30000 Then
		Set KitRetail = 0;
	End If;
    
    /* Don't show cost+ negative discounts */
    If SpecialKitDisc < 0 Then
		Set SpecialKitDisc = 0;
	End If;
    
    If KitDisc < 0 Then
		Set KitDisc = 0;
	End If;
    
    /* Build Header Json */
    Set StringResult = concat('{"customer_price":',IfNull(Convert(KitPrice,CHAR(10) ),'0'),',',
    '"rrp":',IfNull(Convert(KitRRP,CHAR(10) ),'0'),',',
    '"use_bom_price":',UseBOM,',',
    '"discount":',KitDisc,',',
    '"price_no":',KitPriceNo,',',
    '"special_price":',IfNull(Convert(SpecialKitPrice,CHAR(10) ),'0'),',',
    '"special_discount":',IfNull(SpecialKitDisc,'0'),',',
    '"special_price_no":',IfNull(SpecialKitPriceNo,'0'),',',
    '"shopify_rrp":',IfNull(Convert(KitRetail,CHAR(10) ),'0'));
    
    /* Add body */
    Set StringResult = replace(replace(replace(
						concat(StringResult,IfNull(StringBOMResult,'')
		/********** debug only ****************************************/
                        ,',"debug":[{"cust_type":',IfNull(varDebugCustType,'0')
                        ,',"price2":',IfNull(varDebugPrice2,'0')
                        ,',"price3":',IfNull(varDebugPrice3,'0')
                        ,',"baseprice":',IfNull(varDebugBasePrice,'0')
                        , IfNull(concat(Case When varDebugPriceChecks <> '' Then ',' Else '' End ,varDebugPriceChecks),'')	
                        ,'}]'
		/* End debug variables****************************************/
                        
                        
                        ,'}')
                        , '\n', ''), '\r', ''), ' ', '');
   
    
	Return StringResult;
End$$
DELIMITER ;
