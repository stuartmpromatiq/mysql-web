DELIMITER $$
CREATE FUNCTION `fn_PRICE_getDelimitedCustomerSpecialPrice_STAGING`(`inpCustomerCode` VARCHAR(50),  `inpProductCode` VARCHAR(250), `inpQty` int) RETURNS varchar(2000) CHARSET latin1
BEGIN
	Declare StringResult varchar(2000);
	Declare Result Real;
    Declare CustPrice Real;
    Declare RRP Real;  
    Declare CusDiscount Real;
    Declare PriceNo int;
    Declare SpecialPrice real;
    Declare SpecialCusDiscount Real;
    Declare SpecialPriceNo int;
	
    
    Declare varPriceNumber varchar(50);
    Declare varCustomerType int;
    Declare varPriority int;
    Declare varProductType int;
    Declare varPriceType varchar(1);
    Declare varIsSpecial int;
    Declare varPriceFound bit;
    Declare varSpecialFound bit;
    Declare varCustomerDiscount float;
    Declare varTempDiscount float;
    Declare varPriceCode int;
    Declare varPriceKey3 varchar(1);
    Declare varWarehouse varchar(20);
    Declare varBillTo varchar(50);
    Declare varUseBillTo varchar(10);
    Declare varTaxApply float;
    Declare done Int DEFAULT FALSE;
    
    
    /**** Debug options only ***********/
	Declare BasePrice Real;
    Declare BasePrice1 Real;
    Declare BasePrice2 Real;
    Declare varCriteriaMet varchar(10);
    Declare varDebugJSON varchar(2000);
    /* **********************************/


	Declare PC Cursor For 
	SELECT h.PriceNumber, h.Priority, h.CustomerType, h.ProductType, h.PriceType, IfNull(h.PriceKey3,''), IfNull(s.SpecialPrice,0)
	  FROM SWD_PRICE_Hierarchy h 
      Left Join SWD_PRICE_SpecialIndicator s On h.PriceNumber = s.PriceNumber
	  Order By h.Priority;
      
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;  
      
	Set Result = NULL;
    Set CustPrice = NULL;
    Set CusDiscount = 0;
    Set PriceNo = 0;
    Set RRP = 0;
    
	Set varCustomerDiscount = 0;
	Set varTaxApply = 10;
    Set varPriceFound = 0 ;
    Set varBillTo = '';
    Set varIsSpecial = 0;
    Set varSpecialFound = 0;
    
    -- Set default qty
    if inpQty Is Null Then
		Set inpQty = 1;
	End if;
    
	/*Get customer specific details (look for a field to determine if tax needs to be applied  - defaults to yes) */
	Set varWarehouse = '';
	If inpCustomerCode <> '' Then
		Select price_code, TRIM(warehouse), bill_to, dr_price_disc_by_bill_to Into  varPriceCode , varWarehouse, varBillTo, varUseBillTo  From SWD_Debtor_Master Where accountcode = inpCustomerCode;
	End If;
    
    /* Update debtor account if bill to is required */
    If varUseBillTo = 'Y' And inpCustomerCode <> varBillTo And varBillTo <> '' Then
		Set inpCustomerCode = varBillTo;
        Select price_code, warehouse Into  varPriceCode , varWarehouse  From SWD_Debtor_Master Where accountcode = inpCustomerCode;
    
    End If;
    
    /* Get RRP*/
    Select Max(IfNull(prc_recommend_retail,prc_break_price3))  Into RRP
		FROM SWD_Stock_Price
		Where prc_region_code = ''
		And stock_code = inpProductCode;
 
	/***************** Debugging Only **************/
	Set varDebugJSON = '';
	/***********************************************/
 
	Open PC;
    read_loop: LOOP
		FETCH PC INTO varPriceNumber, varPriority, varCustomerType, varProductType, varPriceType , varPriceKey3, varIsSpecial;
		IF done THEN 
			LEAVE read_loop;
		END IF;
        /***************** Debugging Only **************/
        Set varCriteriaMet = 'N';
        /***********************************************/
        
		If varPriceFound = 0 THEN 
			
            Set varTempDiscount = 0;
			/*Work out price
			-- Direct Price */
			If varCustomerType = 1 and varProductType = 1 And varPriceType In ('P','') And CustPrice Is Null Then
			Select Max(Case When inpQty <= sp_qty_break1 Then prc_break_price1
							When inpQty <= sp_qty_break2 Then prc_break_price2
                            When inpQty <= sp_qty_break3 Then prc_break_price3 Else prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price 
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (sp_key_1 = inpCustomerCode  Or sp_key_1 = '')
				  And (sp_key_2 = inpProductCode )
				  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now() 
				  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );
				-- Debug
				Set varCriteriaMet = '11P';	
            
			END IF;
            
			/* Company Contract Price */
			If varCustomerType = 2 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = '' And CustPrice Is Null Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_marketing_flag = p.sp_key_1
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_2 = inpProductCode )
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );
                  
				-- Debug
               Set varCriteriaMet = '21P';	
               
			END IF;
            
            
            /* Company Price Mask */
			If varCustomerType = 3 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = '' And CustPrice Is Null  And Result Is Null Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_company_mask = p.sp_key_1
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_2 = inpProductCode )
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );
            
				-- Debug
               Set varCriteriaMet = '31P';	
            
			END IF;

			
            /* Company Industry Code */
			If varCustomerType = 4 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = '' And CustPrice Is Null  And Result Is Null Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_industry_code = p.sp_key_1
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_2 = inpProductCode )
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty ); 

 				-- Debug
				Set varCriteriaMet = '41P';	             
            
			END IF;
            
            
             /* Direct Price by warehouse */
			If varCustomerType = 5 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = '' And CustPrice Is Null  And Result Is Null Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (p.sp_key_1 = varWarehouse Or p.sp_key_1 = '')
				  And (p.sp_key_2 = inpProductCode )
                  And (p.sp_key_3 = '')
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );
            
				-- Debug
               Set varCriteriaMet = '51P';	
            
			END IF;

			
			/* Direct Price by warehouse by company Price Mask */
			If varCustomerType = 5 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = '3' And CustPrice Is Null  And Result Is Null Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_company_mask = p.sp_key_3
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_1 = varWarehouse )
                  And (p.sp_key_2 = inpProductCode )
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );

				-- Debug
               Set varCriteriaMet = '51P3';	            
            
			END IF;

			
            /* Direct Price by warehouse by company Industry Code */
			If varCustomerType = 5 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = '4' And CustPrice Is Null  And Result Is Null Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_industry_code = p.sp_key_3
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_1 = varWarehouse )
                  And (p.sp_key_2 = inpProductCode )
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );

 				-- Debug
				Set varCriteriaMet = '51P4';	            
            
			END IF;
			


			/* Company Price Mask by Warehouse */
			If varCustomerType = 3 and varProductType = 1 And varPriceType In ('P','') And varPriceKey3 = 'W' And CustPrice Is Null And Result Is Null  Then
            Select Max(Case When inpQty <= p.sp_qty_break1 Then p.prc_break_price1
							When inpQty <= p.sp_qty_break2 Then p.prc_break_price2
                            When inpQty <= p.sp_qty_break3 Then p.prc_break_price3 Else p.prc_break_price1 End) Into Result
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_company_mask = p.sp_key_1
				Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'P'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_2 = inpProductCode )
                  And (p.sp_key_3 = varWarehouse)                  
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now()
                  -- Add price break
                  And (IfNull(sp_min_qty,0) = 0 Or inpQty >= sp_min_qty );

				-- Debug
                Set varCriteriaMet = '31PW';	            
            
			END IF;
			
			/*######################### Discount ###########################
			-- Direct Discount  */
			If varCustomerType = 1 and varProductType = 1 And varPriceType In ('D','') And CustPrice Is Null  And Result Is Null Then
			Select Max(case when c.price_code = '2' then sp.prc_break_price2 Else sp.prc_break_price3 End * 
			 (1 - p.prc_break_price1/100.0))  , Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.accountcode = p.sp_key_1 
                Inner Join SWD_Stock_Price sp On sp.stock_code = p.sp_key_2 And sp.prc_region_code = ''
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'D'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_2 = inpProductCode )
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '11D';	                  
			
            END IF;
            
            
            /*-- Customer to product group Discount  */
			If varCustomerType = 1 and varProductType = 2 And varPriceType In ('D','') And CustPrice Is Null  And Result Is Null Then
			Select Max(case when c.price_code = '2' then sp.prc_break_price2 Else sp.prc_break_price3 End * 
			 (1 - p.prc_break_price1/100.0))  , Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.accountcode = p.sp_key_1 
                Inner Join SWD_Stock_Master s On s.stock_group = p.sp_key_2
                Inner Join SWD_Stock_Price sp On sp.stock_code = s.stock_code And sp.prc_region_code = ''
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'D'
				  And (c.accountcode = inpCustomerCode )
				  And (s.stock_code = inpProductCode )
                  And IfNull(p.sp_start_date,'1900-01-01') <=  now() And IfNull(p.sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '12D';	                  
			
            END IF;

			
			/*-- Company Contract discount direct product  */
			If varCustomerType = 2 and varProductType = 1 And varPriceType In ('D','') And CustPrice IS Null  And Result Is Null Then
			Select Max(case when c.price_code = '2' then sp.prc_break_price2 Else sp.prc_break_price3 End * 
			 (1 - p.prc_break_price1/100.0))  , Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_marketing_flag = p.sp_key_1
                Inner Join SWD_Stock_Price sp On p.sp_key_2 = sp.stock_code And sp.prc_region_code = ''
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'D'
				  And (c.accountcode = inpCustomerCode )
				  And (p.sp_key_2 = inpProductCode )
                  And IfNull(p.sp_start_date,'1900-01-01') <=  now() And IfNull(p.sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '21D';	                  
			
            END IF;
            


			/*-- Company mask discount direct product  */
			If varCustomerType = 3 and varProductType = 1 And varPriceType In ('D','') And CustPrice Is Null  And Result Is Null Then
			Select Max(case when c.price_code = '2' then sp.prc_break_price2 Else sp.prc_break_price3 End * 
			 (1 - p.prc_break_price1/100.0))  , Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_company_mask = p.sp_key_1
                Inner Join SWD_Stock_Price sp On p.sp_key_2 = sp.stock_code And sp.prc_region_code = ''
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'D'
				  And (c.accountcode = inpCustomerCode)
				  And (p.sp_key_2 = inpProductCode )
                  And IfNull(p.sp_start_date,'1900-01-01') <=  now() And IfNull(p.sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '31D';	
                
            END IF;
			
            
            
            /*-- Company Contract discount product group  */
			If varCustomerType = 2 and varProductType = 2 And varPriceType In ('D','') And CustPrice Is Null  And Result Is Null Then
			Select Max(case when c.price_code = '2' then sp.prc_break_price2 Else sp.prc_break_price3 End * 
			 (1 - p.prc_break_price1/100.0))  , Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_marketing_flag = p.sp_key_1
                Inner Join SWD_Stock_Master s On s.stock_group = p.sp_key_2
                Inner Join SWD_Stock_Price sp On sp.stock_code = s.stock_code And sp.prc_region_code = ''
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'D'
				  And (c.accountcode = inpCustomerCode )
				  And (s.stock_code = inpProductCode )
                  And IfNull(p.sp_start_date,'1900-01-01') <=  now() And IfNull(p.sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '22D';	                  
			
            END IF;           
            
            
            
            /*-- Company mask discount to product group  */
			If varCustomerType = 3 and varProductType = 2 And varPriceType In ('D','') And CustPrice Is Null  And Result Is Null Then
			Select Max(case when c.price_code = '2' then sp.prc_break_price2 Else sp.prc_break_price3 End * 
			 (1 - p.prc_break_price1/100.0))  , Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Debtor_Master c On c.dr_company_mask = p.sp_key_1
                Inner Join SWD_Stock_Master s On s.stock_group = p.sp_key_2
                Inner Join SWD_Stock_Price sp On sp.stock_code = s.stock_code And sp.prc_region_code = ''
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'D'
				  And (c.accountcode = inpCustomerCode )
				  And (s.stock_code = inpProductCode )
                  And IfNull(p.sp_start_date,'1900-01-01') <=  now() And IfNull(p.sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '32D';	                  
			
            END IF;   
            
            
            /*######################### Cost Plus ###########################
            /* Cost Plus by Product Group */
			If varCustomerType = 1 and varProductType = 2 And varPriceType In ('C','')  And CustPrice Is Null  And Result Is Null Then
			Select Max(sw.whse_avg_cost) * (1.00 +  Max(p.prc_break_price1)/100.00), -1 *  Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
				Inner Join SWD_Stock_Master s On s.stock_group = p.sp_key_2
				Inner Join SWD_Stock_Warehouse sw On s.stock_code = sw.stock_code And sw.whse_code = varWarehouse
				  Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'C'
				  And (p.sp_key_1 = inpCustomerCode)
				  And (s.stock_code = inpProductCode)
				  And IfNull(p.sp_start_date,'1900-01-01') <=  now() And IfNull(p.sp_end_date,'2079-01-01') >= now();

				-- Debug
                Set varCriteriaMet = '12C';		
                
			END IF;   
            
            /* Direct Price by warehouse */
			If varCustomerType = 5 and varProductType = 1 And varPriceType In ('C','') And varPriceKey3 = '' And CustPrice Is Null  And Result Is Null Then
            Select Max(sw.whse_avg_cost) * (1.00 +  Max(p.prc_break_price1)/100.00), -1 *  Max(p.prc_break_price1) Into Result, varTempDiscount
				FROM SWD_Stock_Special_Retail_Price p
                Inner Join SWD_Stock_Master s On s.stock_code = p.sp_key_2
				Inner Join SWD_Stock_Warehouse sw On s.stock_code = sw.stock_code And sw.whse_code = varWarehouse
                Where sp_key_type = varPriceNumber
				  And sp_price_or_disc1 = 'C'
				  And (p.sp_key_1 = varWarehouse Or p.sp_key_1 = '')
				  And (p.sp_key_2 = inpProductCode )
                  And (p.sp_key_3 = '')
                  And IfNull(sp_start_date,'1900-01-01') <=  now() And IfNull(sp_end_date,'2079-01-01') >= now();
            
				-- Debug
               Set varCriteriaMet = '51C';	
            
			END IF;

			/*****  Debug only ********************************/
            if varDebugJSON = '' then
				Set varDebugJSON = '"pricechecks"#[';
			else 
				Set varDebugJSON = Concat(varDebugJSON,'|');
			end if;
            
            Set varDebugJSON = Concat(varDebugJSON,'{"prno"#',IfNull(Convert(varPriceNumber ,CHAR(6) ),'0'),'|"criteria"#"',varCriteriaMet, '"|"price"#"',IfNull(Convert(Result,CHAR(10) ),'NULL') , '"|"disc"#',IfNull(Convert(varTempDiscount ,CHAR(6) ),'0') , '|"special"#',IfNull(Convert(varIsSpecial ,CHAR(6) ),'0'),'}' );
        
            
            
            /**************************************************/
            
			-- Check if price has been found
			If Result Is Not Null Then 
				-- Check for specials
                If varIsSpecial = 1 Then
					Set varSpecialFound = 1;
                    -- Only set special if regular price was not found higher up the priority list
					If SpecialPrice Is Null And CustPrice Is Null Then						
						Set SpecialPrice = Result;
						Set SpecialPriceNo = varPriceNumber;
						Set SpecialCusDiscount = varTempDiscount;
					End If;
                    
                Else     
					-- Always find regular price to show on the web
					Set varPriceFound = 1;
					Set PriceNo = varPriceNumber;
					Set CusDiscount = varTempDiscount;
                    Set CustPrice = Result;
				End If;
			END IF;  
            
		End If;
		Set Result = Null;

	END LOOP read_loop;
	CLOSE PC;

	/* Add default prices where they are not set as specials */
	If inpCustomerCode <> '' And varPriceFound = 0 Then
	
		
		Select Max(case when varPriceCode = '3' then prc_break_price3 Else prc_break_price2 End * 
			 (1 - varCustomerDiscount/100.0))  Into CustPrice
             FROM SWD_Stock_Price
				Where prc_region_code = ''
				And stock_code = inpProductCode;
				  
		Set CusDiscount = varCustomerDiscount;

	END IF;
    
    /* Debug Only    */
	-- Get base price        
	Select Max(case when varPriceCode = '2' then prc_break_price2 Else prc_break_price3 End),
		Max(prc_break_price2),
		Max(prc_break_price3)
	FROM SWD_Stock_Price
		Where prc_region_code = ''
		And stock_code = inpProductCode
		Into BasePrice, BasePrice1, BasePrice2     ;
    /****************************************************************/
    

	If SpecialPrice Is Null Then
		Set SpecialPrice = 0;
	End If;

	/* Don't allow > 25000 */
    If CustPrice > 20000 Then
		Set CustPrice = 0;
	END IF; 
    If RRP > 30000 Then
		Set RRP = 0;
	END IF; 
    If SpecialPrice > 20000 Then
		Set SpecialPrice = 0;
	END IF; 
    

    -- Change rule to use special regardless of whether it is higher than the customer price as it will be higher in the priority
    If SpecialPrice >= CustPrice  Then
		Set CustPrice = 0;
        Set CusDiscount = 0;
    
    End If;
    
	
	/* Add GST: 10% */
    Set CustPrice = Round(CustPrice * 1.1,2);
    Set RRP = Round(RRP * 1.1,2);
    Set SpecialPrice = Round(SpecialPrice * 1.1,2);
    
    Set StringResult = Concat('CUSTPR:',IfNull(Convert(CustPrice,CHAR(10) ),'0'),',','RRP:',IfNull(Convert(RRP ,CHAR(10) ),'0') ,',','DISC:',IfNull(Convert(CusDiscount ,CHAR(6) ),'0') ,',','PRNO:',IfNull(Convert(PriceNo ,CHAR(6) ),'0')
    ,',','SPPR:',IfNull(Convert(SpecialPrice ,CHAR(10) ),'0') ,',','SPDISC:',IfNull(Convert(SpecialCusDiscount ,CHAR(6) ),'0') ,',','SPPRNO:',IfNull(Convert(SpecialPriceNo ,CHAR(6) ),'0') 
    ,',','CUST_TYPE:',varPriceCode,', PRICE2:',IfNull(Convert(BasePrice1,CHAR(10) ),'0'),', PRICE3:',IfNull(Convert(BasePrice2,CHAR(10) ),'0'),', BASE_USED:',IfNull(Convert(BasePrice,CHAR(10) ),'0')
    , ',','DEBUGJSON:',varDebugJSON,']'
    ); 
	
	Return StringResult;
End$$
DELIMITER ;
